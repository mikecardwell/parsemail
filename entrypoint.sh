#!/usr/bin/env bash
set -e

# Write SECRET_KEY environment variable to the settings file
grep -v SECRET_KEY parsemail/settings.py > parsemail/settings.py.tmp \
 && mv parsemail/settings.py.tmp parsemail/settings.py \
 && echo "SECRET_KEY = '${SECRET_KEY}'" >> parsemail/settings.py \
 && sed -i -E "s/^ALLOWED_HOSTS = .+/ALLOWED_HOSTS = ['*']/" parsemail/settings.py \

./scripts/publicsuffix.py
./scripts/geoip.sh

function expire_loop {
    while :; do
        sleep 60
        ./scripts/expire.py
    done
}

expire_loop &

# Start parsemail
echo "Starting parsemail"
gunicorn3 parsemail.wsgi --bind 0.0.0.0:8000
