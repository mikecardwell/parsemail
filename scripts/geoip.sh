#!/usr/bin/env bash
set -e

# Maxmind db
mkdir -p /geoip
if [[ -f "/geoip/GeoLite2-City.mmdb" ]]; then
    : # File already exists. Do nothing
elif [[ "$MAXMIND_LICENSE" != "" ]]; then
    # A Maxmind license was supplied. Download it
    echo "Downloading GeoLite2-City from Maxmind"
    curl -sL "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=$MAXMIND_LICENSE&suffix=tar.gz" | tar xzf -
    mv GeoLite2-City_*/GeoLite2-City.mmdb /geoip/
    rm -rf GeoLite2-City_*
else
    # Use this third party source. Not ideal, but better than nothing
    echo "Downloading GeoLite2-City from git.io"
    curl -sL https://git.io/GeoLite2-City.mmdb -o /geoip/GeoLite2-City.mmdb
fi