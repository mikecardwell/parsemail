FROM debian:12-slim

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get -yq dist-upgrade \
 && DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
        curl \
        imagemagick \
        python3-django \
        python3-wand \
        python3-geoip2 \
        python3-regex \
        python3-requests \
        gunicorn3 \
        wkhtmltopdf \
        xvfb \
 && apt-get remove -yq git \
 && apt-get autoremove -yq \
 && apt-get clean \
 && rm --force --recursive /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY . /opt/parsemail
WORKDIR /opt/parsemail

RUN mkdir emails resources \
 && ./scripts/publicsuffix.py \
 && python3 manage.py migrate

ENTRYPOINT ["./entrypoint.sh"]
