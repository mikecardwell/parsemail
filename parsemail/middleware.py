class SecurityHeadersMiddleware:
    def process_response(self, request, response):
        response['Content-Security-Policy'] = "require-trusted-types-for 'script'; default-src 'none'; script-src 'self' 'unsafe-inline' https://www.grepular.com; style-src 'self' 'unsafe-inline'; font-src 'self'; img-src 'self'"
        response['Cache-Control']           = "public"
        response['Referrer-Policy']         = "no-referrer"
        response['X-Frame-Options']         = "DENY"
        response['X-Content-Type-Options']  = "nosniff"
        response['Permissions-Policy']      = "interest-cohort=()" # Suck it, Google
        return response
