from django.conf.urls import url
from django.contrib import admin
from parsemail import views

urlpatterns = [
    url(r'^$',        views.home,    name='home'),
   
    url(r'^csp$',     views.csp,     name='csp'),

    url(r'^about$',   views.about,   name='about'),
    
    url(r'^privacy$', views.privacy, name='privacy'),
    
    # /msg/CODE
    url(r'^msg/(?P<code>[a-zA-Z0-9]+)$',
            views.msg,      name='msg'),

    # /msg/CODE/raw
    url(r'^msg/(?P<code>[a-zA-Z0-9]+)/raw$',
            views.msg_raw,  name='msg_raw'),

    # /msg/CODE/ID.headers
    url(r'^msg/(?P<code>[a-zA-Z0-9]+)/(?P<id>[0-9]+(?:\.[0-9]+)*)\.headers$',
            views.msg_headers, name='msg_part_headers'),

    # /msg/CODE.headers
    url(r'^msg/(?P<code>[a-zA-Z0-9]+)\.headers$',
            views.msg_headers, name='msg_headers'),

    # /msg/CODE/ID.txt (The .txt is ignored unless it's a HTML part)
    url(r'^msg/(?P<code>[a-zA-Z0-9]+)/(?P<id>[0-9]+(?:\.[0-9]+)*)\.(?P<ext>txt)$',
            views.msg_part, name='msg_part_ext'),

    # /msg/CODE/ID
    url(r'^msg/(?P<code>[a-zA-Z0-9]+)/(?P<id>[0-9]+(?:\.[0-9]+)*)$',
            views.msg_part, name='msg_part'),

    # Static files
    url(r'^(?P<path>.*\.(css|eot|svg|ttf|txt|woff|gif|js))$', views.serve_htdocs_file),
]