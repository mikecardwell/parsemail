* Author - Mike Cardwell - https://grepular.com/
* Licensing - GPL v3 - Please see COPYING.txt
* Copyright (&copy;) Mike Cardwell

## What is this?

This is the source code for the app at https://www.parsemail.org/ so you can install a locally hosted version if you want.

## How to run

```
$ docker run \
    --network host \
    --rm \
    -e SECRET="A random string for Django" \
    grepular/parsemail:latest
$ open http://127.0.0.1:8000
```

When displaying IP addresses, Parsemail shows the flag of the country it thinks the IP is from. In order to do this it needs a copy of the Maxmind Geo Lite db. You can
supply that to parsemail in one of three ways:

1. Place it at /geoip/GeoLite2-City.mmdb
2. Put your free Maxmind license in the MAXMIND_LICENSE environment variable
3. Do nothing and it will attempt to download from a third party source

## Modifying the css

Install bourbon in the resources dir *if* you wish to modify the CSS

cd resources
bourbon install
cd ..